package br.edu.cest.pet.entity.staff;

import br.edu.cest.pet.entity.EnCategoria;
import br.edu.cest.pet.vo.EnTurno;

public class Veterinario extends Funcionario {

	public Veterinario(String nome, EnTurno turno, String registro) {
		super(nome, turno, registro);
	}
	
	public void setCategoria(EnCategoria cat) {
		this.categoria = cat;
	}
	
	public EnCategoria getCategoria() {
		return categoria;
	}
}
