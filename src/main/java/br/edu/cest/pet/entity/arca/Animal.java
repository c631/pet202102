package br.edu.cest.pet.entity.arca;

public class Animal {
	//nome, sexo, raca, especie, fala
	
	private String nome;
	private String especie;
		
	public Animal(String nome, String especie) {
		super();
		this.nome = nome;
		this.especie = especie;
	}

	private String tipoLocomocao; // Bipede ou Quadrupede
	
	public String getTipoLocomocao() {
		return tipoLocomocao;
	}
	
	public void setTipoLocomocao(String tipoLocomocao) {
		this.tipoLocomocao = tipoLocomocao;
	}
	
	@Override
	public String toString() {
		return nome.concat(" o ".concat(especie));
	}
	
	
}
