package br.edu.cest.pet.vo;

public enum EnumLocomocao {
	BIPEDEDE(1), QUADRUPEDE(2);

	public int val;

	private EnumLocomocao(int val) {
		this.val = val;
	}
}
