package br.edu.cest.pet.divisao;

import java.util.ArrayList;

import br.edu.cest.pet.entity.staff.Atendente;
import br.edu.cest.pet.entity.staff.Funcionario;

public class Atendimento extends Setor {
	private ArrayList<Funcionario> staff = new ArrayList<Funcionario>();

	@Override
	public String getDivisao() {
		return "Atendimento";
	}


	@Override
	public ArrayList<Funcionario> getStaff() {
		return staff;
	}
	
	public void addColaborador(Atendente at) {
		staff.add(at);
	}

}
